From 20680fa928ab9f3437d25c6115e8005ec3d6dd22 Mon Sep 17 00:00:00 2001
From: Dor Askayo <dor.askayo@gmail.com>
Date: Fri, 26 May 2023 14:19:23 +0300
Subject: [PATCH] meson: add feature option for use of system Clang headers at
 runtime

Enabling or disabling the "microsoft-clc" option previously changed
shared logic for all compiler/clc users, which was surprising.

In addition, the option to avoid the use of system Clang headers at
runtime is useful outside the scope of Windows.

Separating the two concepts by making this a neutral feature option
addresses both matters.

Signed-off-by: Dor Askayo <dor.askayo@gmail.com>
Reviewed-by: Jesse Natalie <jenatali@microsoft.com>
---
 meson_options.txt            | 7 +++++++
 src/compiler/clc/meson.build | 5 +++--
 2 files changed, 10 insertions(+), 2 deletions(-)

diff --git a/meson_options.txt b/meson_options.txt
index ac71cf3abeb..455ae4d29bf 100644
--- a/meson_options.txt
+++ b/meson_options.txt
@@ -183,6 +183,13 @@ option(
   description : 'Link libclc SPIR-V statically.',
 )
 
+option(
+  'opencl-external-clang-headers',
+  type : 'feature',
+  description : 'Use system Clang headers at runtime for OpenCL kernel ' +
+                'compilation.',
+)
+
 option(
   'd3d-drivers-path',
   type : 'string',
diff --git a/src/compiler/clc/meson.build b/src/compiler/clc/meson.build
index 64f2e1f6816..80e21b10f2c 100644
--- a/src/compiler/clc/meson.build
+++ b/src/compiler/clc/meson.build
@@ -28,8 +28,9 @@ files_libclc = files(
 _libclc_cpp_args = ['-DLLVM_LIB_DIR="@0@"'.format(llvm_libdir)]
 _libclc_sources = []
 
-if with_microsoft_clc
-
+if get_option('opencl-external-clang-headers') \
+   .disable_auto_if(host_machine.system() == 'windows') \
+   .disabled()
   # LLVM 16 moved clang header path from using full version to only major version
   if dep_llvm.version().version_compare('< 16')
     # Prior to LLVM 16, this path used a full version
-- 
2.40.1

